﻿namespace EmailNotifier
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.tEmail = new System.Windows.Forms.Label();
            this.lNextRun = new System.Windows.Forms.Label();
            this.lLastRun = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tTime = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.tSDB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cSsl = new System.Windows.Forms.CheckBox();
            this.tPassword = new System.Windows.Forms.TextBox();
            this.tUserName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.tReceiver = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tCC = new System.Windows.Forms.TextBox();
            this.tSender = new System.Windows.Forms.TextBox();
            this.tSMTP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.tAction = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tProgress = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(5, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(477, 398);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.GroupBox1);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.tTime);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.pDatePicker);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.tSDB);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(469, 372);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(42, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 23;
            this.button1.Text = "Test SDB";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.tEmail);
            this.GroupBox1.Controls.Add(this.lNextRun);
            this.GroupBox1.Controls.Add(this.lLastRun);
            this.GroupBox1.Controls.Add(this.label12);
            this.GroupBox1.Controls.Add(this.label13);
            this.GroupBox1.Controls.Add(this.label14);
            this.GroupBox1.Location = new System.Drawing.Point(26, 264);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(425, 95);
            this.GroupBox1.TabIndex = 22;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Action Information";
            this.GroupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // tEmail
            // 
            this.tEmail.AutoSize = true;
            this.tEmail.Location = new System.Drawing.Point(20, 29);
            this.tEmail.Name = "tEmail";
            this.tEmail.Size = new System.Drawing.Size(0, 12);
            this.tEmail.TabIndex = 29;
            // 
            // lNextRun
            // 
            this.lNextRun.AutoSize = true;
            this.lNextRun.Location = new System.Drawing.Point(113, 71);
            this.lNextRun.Name = "lNextRun";
            this.lNextRun.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lNextRun.Size = new System.Drawing.Size(0, 12);
            this.lNextRun.TabIndex = 28;
            this.lNextRun.Click += new System.EventHandler(this.label16_Click);
            // 
            // lLastRun
            // 
            this.lLastRun.AutoSize = true;
            this.lLastRun.Location = new System.Drawing.Point(113, 53);
            this.lLastRun.Name = "lLastRun";
            this.lLastRun.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lLastRun.Size = new System.Drawing.Size(0, 12);
            this.lLastRun.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 71);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(89, 12);
            this.label12.TabIndex = 26;
            this.label12.Text = "Next Run Time:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 53);
            this.label13.Name = "label13";
            this.label13.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label13.Size = new System.Drawing.Size(89, 12);
            this.label13.TabIndex = 25;
            this.label13.Text = "Last Run Time:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 12);
            this.label14.TabIndex = 20;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(258, 143);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 12);
            this.label11.TabIndex = 8;
            this.label11.Text = "Minutes";
            // 
            // tTime
            // 
            this.tTime.Location = new System.Drawing.Point(141, 140);
            this.tTime.Name = "tTime";
            this.tTime.Size = new System.Drawing.Size(97, 21);
            this.tTime.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(46, 143);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 12);
            this.label10.TabIndex = 6;
            this.label10.Text = "Repeat Every:";
            // 
            // pDatePicker
            // 
            this.pDatePicker.Location = new System.Drawing.Point(141, 90);
            this.pDatePicker.Name = "pDatePicker";
            this.pDatePicker.Size = new System.Drawing.Size(200, 21);
            this.pDatePicker.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "Start Time:";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(141, 196);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "Run";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // tSDB
            // 
            this.tSDB.Location = new System.Drawing.Point(141, 43);
            this.tSDB.Name = "tSDB";
            this.tSDB.Size = new System.Drawing.Size(200, 21);
            this.tSDB.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 1;
            this.label8.Text = "SDB:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cSsl);
            this.tabPage2.Controls.Add(this.tPassword);
            this.tabPage2.Controls.Add(this.tUserName);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.tReceiver);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.tCC);
            this.tabPage2.Controls.Add(this.tSender);
            this.tabPage2.Controls.Add(this.tSMTP);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(469, 372);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cSsl
            // 
            this.cSsl.AutoSize = true;
            this.cSsl.Location = new System.Drawing.Point(24, 300);
            this.cSsl.Name = "cSsl";
            this.cSsl.Size = new System.Drawing.Size(108, 16);
            this.cSsl.TabIndex = 16;
            this.cSsl.Text = "SSL Connection";
            this.cSsl.UseVisualStyleBackColor = true;
            // 
            // tPassword
            // 
            this.tPassword.Location = new System.Drawing.Point(150, 124);
            this.tPassword.Name = "tPassword";
            this.tPassword.Size = new System.Drawing.Size(301, 21);
            this.tPassword.TabIndex = 15;
            // 
            // tUserName
            // 
            this.tUserName.Location = new System.Drawing.Point(150, 86);
            this.tUserName.Name = "tUserName";
            this.tUserName.Size = new System.Drawing.Size(301, 21);
            this.tUserName.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "Password:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "User Name:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(229, 329);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 23);
            this.button3.TabIndex = 11;
            this.button3.Text = "Send Test Mail";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tReceiver
            // 
            this.tReceiver.Location = new System.Drawing.Point(151, 255);
            this.tReceiver.Name = "tReceiver";
            this.tReceiver.Size = new System.Drawing.Size(301, 21);
            this.tReceiver.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "Test Email Receiver:";
            // 
            // tCC
            // 
            this.tCC.Location = new System.Drawing.Point(151, 212);
            this.tCC.Name = "tCC";
            this.tCC.Size = new System.Drawing.Size(301, 21);
            this.tCC.TabIndex = 8;
            // 
            // tSender
            // 
            this.tSender.Location = new System.Drawing.Point(151, 165);
            this.tSender.Name = "tSender";
            this.tSender.Size = new System.Drawing.Size(301, 21);
            this.tSender.TabIndex = 7;
            // 
            // tSMTP
            // 
            this.tSMTP.Location = new System.Drawing.Point(150, 47);
            this.tSMTP.Name = "tSMTP";
            this.tSMTP.Size = new System.Drawing.Size(301, 21);
            this.tSMTP.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "CC Address:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sender Address:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "SMTP Server:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(377, 329);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tAction
            // 
            this.tAction.AutoSize = true;
            this.tAction.Location = new System.Drawing.Point(5, 423);
            this.tAction.Name = "tAction";
            this.tAction.Size = new System.Drawing.Size(29, 12);
            this.tAction.TabIndex = 2;
            this.tAction.Text = "Idle";
            this.tAction.Click += new System.EventHandler(this.label1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tProgress
            // 
            this.tProgress.AutoSize = true;
            this.tProgress.Location = new System.Drawing.Point(65, 423);
            this.tProgress.Name = "tProgress";
            this.tProgress.Size = new System.Drawing.Size(0, 12);
            this.tProgress.TabIndex = 29;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 444);
            this.Controls.Add(this.tProgress);
            this.Controls.Add(this.tAction);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Email Notifier";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label tAction;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox tReceiver;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tCC;
        private System.Windows.Forms.TextBox tSender;
        private System.Windows.Forms.TextBox tSMTP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tPassword;
        private System.Windows.Forms.TextBox tUserName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cSsl;
        private System.Windows.Forms.TextBox tSDB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button4;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker pDatePicker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer timer1;
        internal System.Windows.Forms.Label lNextRun;
        internal System.Windows.Forms.Label lLastRun;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label tProgress;
        public System.Windows.Forms.Label tEmail;
    }
}

