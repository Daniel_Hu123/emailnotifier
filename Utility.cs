﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using EmailNotifier;
using System.Configuration;
using System.Data.OleDb;
using EmailNotifier.Model;

namespace EmailNotifier
{
    class Utility
    {

        public static void init(string SDB, DateTime pDatetime, Int32 timeSlot)
        {
            Properties.Settings.Default.SDB = SDB;
            Properties.Settings.Default.StartTime = pDatetime;
            Properties.Settings.Default.TimeSlot = timeSlot;
            Properties.Settings.Default.Save();
            
        }
        public static void start(string lastRun)
        {

            SDB sdb = new SDB();
            sdb.connect(Properties.Settings.Default.SDB);
            sdb.checkActivity(lastRun);
                        
            
        
        }
        public static void checkActivity()
        {
           
            
        
        }
        public static void saveSettings(string SMTP, string Sender, string Receiver, string CC, string UserName, string Password)
        {
            //save SMTP settings
            try
            {
                Properties.Settings.Default.SMTP = SMTP;
                Properties.Settings.Default.Sender = Sender;
                Properties.Settings.Default.Receiver = Receiver;
                Properties.Settings.Default.Cc = CC;
                Properties.Settings.Default.Username = UserName;
                Properties.Settings.Default.Password = Password;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        
        }
        public static String sendemail(string SMTP, string Username, string Password ,string Sender, string Receiver, string Cc, string Content,string subject)
        {    
            //send email function without auth
            try
            {
                SmtpClient smtpc = new SmtpClient();
                smtpc.Host = SMTP;
                smtpc.DeliveryMethod = SmtpDeliveryMethod.Network;
                if (Username != "")  //not implemented yet
                {
                    System.Net.NetworkCredential smtpuser= new System.Net.NetworkCredential();
                    smtpuser.UserName = Username;
                    smtpuser.Password = Password;
                    smtpc.Credentials = smtpuser;
                    smtpc.Port = 465;
                    smtpc.EnableSsl = true; //not implemented yet
            
                }

                MailAddress from = new MailAddress(Sender);
                MailAddress to = new MailAddress(Receiver);
                MailAddress cc = new MailAddress(Cc);
                MailMessage message = new MailMessage(from, to);
                message.Body = Content;
                message.CC.Add(cc);
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                message.Subject = subject;
                message.ReplyTo = from;
           
                smtpc.Send(message);
            }
            catch (Exception ex)
            {
                return(ex.ToString());

            }
            return "Success";

             

        }

        public static void sendNotification(EmailTemplate email)
        {
            //send email notification
            String body;  

            //Email Content
             body = "This email confirms that the course (ref: " + email.HostKey + " " + email.ActName + ") has been modified as below:<br/><br/>" +
            "Course(Code): " + email.HostKey + "<br/>" +
            "Name: " + email.ActDescription + "<br/>" +
   "Datetime: " + email.DateTimes + "<br/>" +
   "Staff: " + email.Staff + "<br/>" +
   "Location: " + email.Location + "<br/>";

         //   Console.WriteLine(email.email + " " + body);

              sendemail(Properties.Settings.Default.SMTP, "", "", Properties.Settings.Default.Sender, email.email, Properties.Settings.Default.Cc, body, "Course has been modified");


        }

        public static OleDbDataReader GetOLEDBRecordSet(string progId, string SQL)
        {

            //Trial for OLEDB

            OleDbDataReader GetOLEDBRecordSet = null;
            try
            {
                OleDbConnection oConn = new OleDbConnection();
                oConn.ConnectionString = "Provider=" + progId + ".OLEDB";
                oConn.Open();
                OleDbCommand oCommand = new OleDbCommand(SQL, oConn);
                OleDbDataReader oReader = oCommand.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
                oCommand = null;
                GetOLEDBRecordSet = oReader;
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }

            return GetOLEDBRecordSet;


        }

        public static Boolean SendSQLOLEDB(string progId, string SQL)
        {

            //Trial for OLEDB

            try
            {
                OleDbConnection oConn = new OleDbConnection();
                oConn.ConnectionString = "Provider=" + progId + ".OLEDB";
                OleDbCommand oCommand = new OleDbCommand(SQL, oConn);
                oConn.Open();
                OleDbDataReader oReader = oCommand.ExecuteReader();
                oCommand = null;
                return true;
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }

            return false;
        }
    }

    

}
