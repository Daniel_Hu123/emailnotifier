﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SplusServer;
using Microsoft.VisualBasic;
using System.Data.OleDb;
using EmailNotifier.Model;
using EmailNotifier;

namespace EmailNotifier
{
    class SDB
    {
        private SplusServer.Application SplusApp;

        private string SDBName;

        
        public void connect(string progId)
        {

            //Connect to image and refersh SDB

            SDBName = progId;

            SplusApp = (SplusServer.Application)Microsoft.VisualBasic.Interaction.CreateObject(SDBName + ".Application");

            if (SplusApp.SDB.IsConnected == false)
                SplusApp.SDB.Connect();
            while (SplusApp.SDB.IsSDBBusy)
            {
                System.Threading.Thread.Sleep(1000);

            }
            SplusApp.SDB.Refresh();
        }

        public void checkActivity(string lastRun)
        {
           //   College college = SplusApp.ActiveCollege;
           //   OleDbDataReader rs;
            
            //  OleDbDataReader rs_dept;   
          
            //   rs = Utility.GetOLEDBRecordSet(SDBName, "select HostKey,count(*) as RowNum from Activity where WhenScheduled>" + lastRun + "");
            
            College college = SplusApp.ActiveCollege;
            foreach (Activity act in college.Activities) //Foreach all activities in image file
            {

                Form1.form1.tProgress.Text = "Checking: "+ act.HostKey;
                
                if (act.SchedulingStatus == cpSchedulingStatusType.cpSchedulingStatusTypeScheduled) //make sure it is scheduled
                { 
                    if (act.WhenScheduled>Convert.ToDateTime(lastRun)) //if it has been scheduled after last run, need to send notification email
                    {

                        foreach (StudentSet sset in act.GetResourceAllocation(cpResourceType.cpResourceTypeStudentSet)) //get all student sets belong to this activity
                        {
                       
                            foreach (Student stu in sset.Students) //get all students belong to this student set
                            {
                                EmailTemplate Email = new EmailTemplate(); 
                                Email.HostKey = act.HostKey;
                                Email.ActName = act.Name;
                                Email.ActDescription = act.Description;
                                String staffname="";
                                foreach (StaffMember staff in act.GetResourceAllocation(cpResourceType.cpResourceTypeStaffMember)) //get all staffs
                                    staffname = staffname + staff.Name + " ";
                                Email.Staff = staffname;
                                String locationname = "";
                                foreach (Location loc in act.GetResourceAllocation(cpResourceType.cpResourceTypeLocation)) //get all locations
                                    locationname = locationname + loc.Name + " ";
                                Email.Location = locationname;
                                String DateTimes = ((Object[])act.ScheduledStartDateTimes)[((Object[])act.ScheduledStartDateTimes).GetLowerBound(0)] + " - " + ((Object[])act.ScheduledFinishDateTimes)[((Object[])act.ScheduledFinishDateTimes).GetUpperBound(0)];
                                //get activity start date/time to end date/time
                                Email.DateTimes = DateTimes;
                                Email.email = stu.Email;
                                Utility.sendNotification(Email); //Send Notification
                                Form1.form1.tEmail.Text = "Sent Email for " + Email.HostKey + " to " + stu.Email;

                            }
                        }

                    }
                }



            }
        }

    }
}
