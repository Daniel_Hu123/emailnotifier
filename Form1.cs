﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmailNotifier
{
    public partial class Form1 : Form
    {

        public static Form1 form1;
        

        public Form1()
        {
            InitializeComponent();
            form1 = this;
            tabPage1.Text = "Configurations";
            tabPage2.Text = "SMTP Settings";
            tSMTP.Text = Properties.Settings.Default.SMTP;
            tSender.Text = Properties.Settings.Default.Sender; 
            tReceiver.Text =Properties.Settings.Default.Receiver;
            tCC.Text =Properties.Settings.Default.Cc;
            tUserName.Text=Properties.Settings.Default.Username;
            tPassword.Text=Properties.Settings.Default.Password;
            pDatePicker.Format = DateTimePickerFormat.Custom;
            pDatePicker.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            timer1.Enabled = false;

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            String SMTP = tSMTP.Text;
            String Sender = tSender.Text;
            String Receiver = tReceiver.Text;
            String Cc = tCC.Text;
            String Content = "Test Mail";
            String username = tUserName.Text;
            String password = tUserName.Text;
            String result = Utility.sendemail(SMTP, username,password, Sender, Receiver, Cc, Content,"Test Mail");
            MessageBox.Show(result);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.SMTP = tSMTP.Text;
            Properties.Settings.Default.Sender = tSender.Text;
            Properties.Settings.Default.Receiver = tReceiver.Text;
            Properties.Settings.Default.Cc = tCC.Text;
            Properties.Settings.Default.Username = tUserName.Text;
            Properties.Settings.Default.Password = tPassword.Text;
            Properties.Settings.Default.Save();

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (tTime.Text == "")
            {

                MessageBox.Show("Please Enter SDB Name.");
            }
            else
            {

                String SDB = tSDB.Text;
           

                DateTime pDateTime = pDatePicker.Value;

                if (tTime.Text != "")
                {
                    int timeSlot = Convert.ToInt32(tTime.Text);

                    Utility.init(SDB, pDateTime, timeSlot);  //save info to the configure file

                    lNextRun.Text = pDateTime.ToString();   //calculate next run time

                    lLastRun.Text = DateTime.Now.ToString();

                    timer1.Enabled = true;
                }
                else
                {

                    MessageBox.Show("Please Enter Time.");

                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

          if (DateTime.Now.ToString()==lNextRun.Text)
            
        //    if (true)
            {
                timer1.Enabled = false;
                Console.WriteLine(DateTime.Now.ToString());
                Console.WriteLine(Convert.ToDateTime(lNextRun.Text).ToString());              
                int timeSlot = Convert.ToInt32(tTime.Text);
                tAction.Text = "Running...";
                Utility.start(lLastRun.Text);
                tAction.Text = "Idle";
                tEmail.Text = "";
                lLastRun.Text = lNextRun.Text;
                lNextRun.Text = DateTime.Now.AddMinutes(timeSlot).ToString();
                timer1.Enabled = true;
              
            }
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                SDB sdb = new SDB();

                sdb.connect(tSDB.Text);

                MessageBox.Show("Connected to SDB Successfuly!");
            }
            catch (Exception ex) {

                MessageBox.Show("Error: "+ex.ToString());
           
            
            }
        }
    }
}
