﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailNotifier.Model
{
    class EmailTemplate
    {
        public  String HostKey { get; set; }
        public  String ActName { get; set; }
        public  String ActDescription { get; set; }
        public  String DateTimes { get; set; }
        public  String Staff { get; set; }
        public  String Location { get; set; }
        public  String email { get; set; }

    }
}
